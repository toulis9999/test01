#ifndef SCENE1_H
#define SCENE1_H

#include "SFML/Graphics.hpp"
#include "scene.h"

class DistScene: public Scene
{
public:
	DistScene();

	void init(sf::RenderWindow& window) override;
	void handleEvents(const sf::Event& event) override;
	void update(const sf::Time& dt) override;
	void draw(sf::RenderWindow& window) override;

private:
	sf::Font font;
	sf::Text text;
	sf::Texture uv;
	sf::Texture tex;
	sf::RectangleShape sp;
	sf::Shader shad;

	float x,a;
};

#endif // SCENE1_H
