#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>

class Settings final
{
public:
	Settings();
	void loadFromFile(const std::string& filename);
	int getWin_height() const;
	int getWin_width() const;
	bool getVsync() const;

private:
	int win_height;
	int win_width;
	bool vsync;
};

#endif // SETTINGS_H
