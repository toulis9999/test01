#include "app.h"

App::App():running(true)
{
	currentScene = 0;
	settings.loadFromFile("resources/foo.xml");

	window.create(sf::VideoMode(settings.getWin_width(), settings.getWin_height()), "My window", sf::Style::Close);
	window.setVerticalSyncEnabled(settings.getVsync());
}

void App::insertScene(std::unique_ptr<Scene> sc)
{
	sc->init(window);
	states.push_back(std::move(sc));
}

void App::run()
{
	sf::Time currenttime = clock.getElapsedTime();
	sf::Time accumulator = sf::Time::Zero;

	//initialise resources for stats display
	fnt.loadFromFile("resources/UMR.ttf");
	txtline.setFont(fnt);
	txtline.setCharacterSize(18);
	txtline.setString("Press + or - to change scenes or Esc to exit");
	txtline.setColor(sf::Color::Black);
	txtline.setPosition(0, window.getSize().y - 20);

	sf::Vector2f rectdim(txtline.getLocalBounds().width, txtline.getLocalBounds().height + 4);
	rectsh.setSize(rectdim);
	rectsh.setFillColor(sf::Color::Green);
	rectsh.setPosition(txtline.getPosition());
	//----------------------------------------

	while (running)
	{
		newtime = clock.getElapsedTime();
		frametime = newtime - currenttime;
		currenttime = newtime;

		accumulator += frametime;


		while (accumulator >= dt)
		{
			//Update subloop
			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
				{
					running = false;
				}
				else if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Add)
				{
					if (currentScene < states.size() -1) currentScene++; // upper bound check for index
				}
				else if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Subtract)
				{
					if (currentScene > 0) currentScene--; // lower bound check for index
				}
				else
				{
					if (!states.empty() && currentScene < states.size())
					{
						states[currentScene]->handleEvents(event);
					}
				}
			}
			if (!states.empty() && currentScene < states.size())
			{
				states[currentScene]-> update(dt);
			}
			accumulator -= dt;
			t +=dt;
		}
		window.clear();

		if (!states.empty() && currentScene < states.size())
		{
			states[currentScene]-> draw(window);
		}

		//---draw stats text
		window.draw(rectsh);
		window.draw(txtline);
		window.display();
	}
	//cleanup
	window.close();
}
