#ifndef TESTSCENE_H
#define TESTSCENE_H

#include "SFML/Graphics.hpp"
#include "scene.h"

class Testscene: public Scene
{
public:
	Testscene();

	void init(sf::RenderWindow& window) override;
	void handleEvents(const sf::Event& event) override;
	void update(const sf::Time& dt) override;
	void draw(sf::RenderWindow& window) override;

private:
	sf::Font font;
	sf::Text text;

};

#endif // TESTSCENE_H
