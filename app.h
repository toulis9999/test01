#ifndef APP_H
#define APP_H

#include <vector>
#include <memory>

#include "SFML/Graphics.hpp"
#include "settings.h"
#include "scene.h"

class App final
{
public:
	App();

	void insertScene(std::unique_ptr<Scene> sc);
	void run();

private:
	unsigned int currentScene; // index to currently active scene
	std::vector<std::unique_ptr<Scene> > states;
	Settings settings;
	bool running;

	sf::Clock clock;

	sf::Time newtime;
	sf::Time frametime;
	sf::Time t;


	const sf::Time dt = sf::seconds(1.0f/60.0f);
	sf::RenderWindow window;

	//resources for stats drawing independent of scene
	sf::RectangleShape rectsh;
	sf::Font fnt;
	sf::Text txtline;
};

#endif // APP_H
