#include "distscene.h"

DistScene::DistScene()
{
}

void DistScene::init(sf::RenderWindow& window)
{
	font.loadFromFile("resources/UMR.ttf");

	text.setFont(font);
	text.setCharacterSize(24);
	text.setString("Use the Left and Right arrow keys to adjust the spninning speed");
	text.setPosition(0,0);
	int fx = (window.getSize().x-text.getGlobalBounds().width) / 2;
	text.setPosition(fx, 5);

	uv.loadFromFile("resources/uv_space.png");
	uv.setSmooth(true);

	tex.loadFromFile("resources/map.png");
	tex.setRepeated(true);
	tex.setSmooth(true);

	sp.setSize(sf::Vector2f(512,512));
	sp.setTexture(&tex);
	int qx = (window.getSize().x-sp.getSize().x) / 2;
	int qy = (window.getSize().y-sp.getSize().y) / 2;
	sp.setPosition(qx,qy);

	shad.loadFromFile("resources/uv_distort.frag", sf::Shader::Fragment);
	shad.setParameter("texture", sf::Shader::CurrentTexture);
	shad.setParameter("coor",uv);

	x = 0.f;
	a = 0.f;
}

void DistScene::handleEvents(const sf::Event& event)
{
	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::Left)
		{
			a += 0.0001;
		}
		if (event.key.code == sf::Keyboard::Right)
		{
			a -=0.0001;
		}
	}
}

void DistScene::update(const sf::Time& dt)
{
	x += a;
}

void DistScene::draw(sf::RenderWindow& window)
{
	shad.setParameter("time",x);

	window.clear();

	window.draw(text);
	window.draw(sp, &shad);
}
