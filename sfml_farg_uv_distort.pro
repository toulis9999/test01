TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11



LIBS += -L/home/toulis/SDKs/SFML/lib

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-network-d -lsfml-window-d -lsfml-system-d

INCLUDEPATH += /home/toulis/SDKs/SFML/include
DEPENDPATH += /home/toulis/SDKs/SFML/include

HEADERS += \
    app.h \
    settings.h \
    tinyxml2/tinyxml2.h \
    scene.h \
    distscene.h \
    testscene.h

SOURCES += main.cpp \
    app.cpp \
    settings.cpp \
    tinyxml2/tinyxml2.cpp \
    gamestate.cpp \
    distscene.cpp \
    testscene.cpp

OTHER_FILES += \
    resources/uv_distort.frag
