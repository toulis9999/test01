#include "settings.h"
#include "tinyxml2/tinyxml2.h"

Settings::Settings():win_height(600),win_width(800),vsync(true)
{
}

void Settings::loadFromFile(const std::string& filename)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(filename.c_str());
	int h = doc.FirstChildElement()->IntAttribute("height");
	int w = doc.FirstChildElement()->IntAttribute("width");
	bool sync = doc.FirstChildElement()->BoolAttribute("vsync");
	vsync = sync;
	win_height = h;
	win_width = w;
}
int Settings::getWin_height() const
{
	return win_height;
}

int Settings::getWin_width() const
{
	return win_width;
}

bool Settings::getVsync() const
{
	return vsync;
}



