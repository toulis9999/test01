#include <memory>

#include "app.h"
#include "scene.h"
#include "distscene.h"
#include "testscene.h"

int main()
{
	App app;
	std::unique_ptr<Scene> test1(new DistScene);
	std::unique_ptr<Scene> test2(new Testscene);

	app.insertScene(std::move(test1));
	app.insertScene(std::move(test2));
	app.run();

}

