#ifndef SCENE_H
#define SCENE_H

#include <memory>
#include "SFML/Graphics.hpp"

class Scene
{
public:
	Scene();
	virtual ~Scene();
	virtual void init(sf::RenderWindow& wind) = 0;
	virtual void handleEvents(const sf::Event& event) = 0;
	virtual void update(const sf::Time& dt) = 0;
	virtual void draw(sf::RenderWindow& window) = 0;
};

#endif // SCENE_H
