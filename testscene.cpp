#include "testscene.h"

Testscene::Testscene()
{
}

void Testscene::init(sf::RenderWindow& window)
{
	font.loadFromFile("resources/UMR.ttf");

	text.setFont(font);
	text.setColor(sf::Color::Red);
	text.setCharacterSize(28);
	text.setString("Test Scene. Nothing to see here yet!");
	text.setPosition(0,0);
	int fx = (window.getSize().x-text.getGlobalBounds().width) / 2;
	text.setPosition(fx, window.getSize().y / 2);
}

void Testscene::handleEvents(const sf::Event& event)
{

}

void Testscene::update(const sf::Time& dt)
{

}

void Testscene::draw(sf::RenderWindow& window)
{
	window.draw(text);
}
